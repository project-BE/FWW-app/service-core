-- public.airports definition

-- Drop table

-- DROP TABLE public.airports;

CREATE TABLE public.airports (
	airport_id int4 NOT NULL,
	airport_code varchar(50) NOT NULL,
	airport_name varchar(50) NOT NULL,
	citycode varchar(50) NOT NULL,
	cityname varchar(50) NOT NULL,
	countryname varchar(50) NOT NULL,
	countrycode varchar(50) NOT NULL,
	timezone int4 NOT NULL,
	lat float4 NOT NULL,
	lon float4 NOT NULL,
	numairports int4 NULL,
	city bool NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	CONSTRAINT airports_pk PRIMARY KEY (airport_id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public.airports for each row execute function trigger_set_timestamp();


-- public.passenger definition

-- Drop table

-- DROP TABLE public.passenger;

CREATE TABLE public.passenger (
	passenger_id serial4 NOT NULL,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	email varchar(100) NOT NULL,
	phone_number varchar(20) NOT NULL,
	nik varchar NOT NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	boarding_status varchar NULL,
	CONSTRAINT passenger_pkey PRIMARY KEY (passenger_id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public.passenger for each row execute function trigger_set_timestamp();


-- public.plane definition

-- Drop table

-- DROP TABLE public.plane;

CREATE TABLE public.plane (
	plane_id serial4 NOT NULL,
	plane_name varchar(100) NOT NULL,
	model varchar(50) NOT NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	capacity int4 NOT NULL,
	CONSTRAINT plane_pkey PRIMARY KEY (plane_id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public.plane for each row execute function trigger_set_timestamp();


-- public."user" definition

-- Drop table

-- DROP TABLE public."user";

CREATE TABLE public."user" (
	id serial4 NOT NULL,
	username varchar NOT NULL,
	email varchar NOT NULL,
	"password" varchar NOT NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	user_type varchar NULL,
	blacklist_status bool NULL DEFAULT false,
	CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public."user" for each row execute function trigger_set_timestamp();


-- public.baggage definition

-- Drop table

-- DROP TABLE public.baggage;

CREATE TABLE public.baggage (
	baggage_id serial4 NOT NULL,
	passenger_id int4 NOT NULL,
	weight numeric(5, 2) NULL,
	description text NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	baggage_status varchar NULL,
	CONSTRAINT baggage_pkey PRIMARY KEY (baggage_id),
	CONSTRAINT baggage_passenger_id_fkey FOREIGN KEY (passenger_id) REFERENCES public.passenger(passenger_id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public.baggage for each row execute function trigger_set_timestamp();


-- public.flight definition

-- Drop table

-- DROP TABLE public.flight;

CREATE TABLE public.flight (
	flight_id serial4 NOT NULL,
	flight_code varchar NOT NULL,
	departure_airport_id int4 NOT NULL,
	destination_airport_id int4 NOT NULL,
	departure_time timestamptz NOT NULL,
	arrival_time timestamptz NOT NULL,
	capacity int4 NOT NULL,
	plane_id int4 NOT NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	CONSTRAINT flight_pkey PRIMARY KEY (flight_id),
	CONSTRAINT fk_departure_airport FOREIGN KEY (departure_airport_id) REFERENCES public.airports(airport_id),
	CONSTRAINT fk_destination_airport FOREIGN KEY (destination_airport_id) REFERENCES public.airports(airport_id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public.flight for each row execute function trigger_set_timestamp();


-- public.reservation definition

-- Drop table

-- DROP TABLE public.reservation;

CREATE TABLE public.reservation (
	reservation_id serial4 NOT NULL,
	flight_id int4 NOT NULL,
	passenger_id int4 NOT NULL,
	transaction_id varchar(30) NULL,
	booking_code varchar(30) NULL,
	reservation_status varchar(10) NULL,
	seat_id int4 NOT NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	user_id int4 NULL,
	CONSTRAINT reservation_pkey PRIMARY KEY (reservation_id),
	CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public."user"(id),
	CONSTRAINT reservation_flight_id_fkey FOREIGN KEY (flight_id) REFERENCES public.flight(flight_id),
	CONSTRAINT reservation_passenger_id_fkey FOREIGN KEY (passenger_id) REFERENCES public.passenger(passenger_id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public.reservation for each row execute function trigger_set_timestamp();


-- public.seat definition

-- Drop table

-- DROP TABLE public.seat;

CREATE TABLE public.seat (
	seat_id serial4 NOT NULL,
	flight_id int4 NULL,
	price numeric(10, 2) NOT NULL,
	seat_status varchar NULL DEFAULT 'IDLE'::character varying,
	seat_number int4 NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	CONSTRAINT seat_pkey PRIMARY KEY (seat_id),
	CONSTRAINT seat_flight_id_fkey FOREIGN KEY (flight_id) REFERENCES public.flight(flight_id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public.seat for each row execute function trigger_set_timestamp();


-- public.payment definition

-- Drop table

-- DROP TABLE public.payment;

CREATE TABLE public.payment (
	payment_id serial4 NOT NULL,
	reservation_id int4 NOT NULL,
	payment_date timestamptz NULL,
	amount numeric(10, 2) NULL DEFAULT NULL::numeric,
	payment_method varchar(50) NULL DEFAULT NULL::character varying,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	reservation_code varchar NULL,
	CONSTRAINT payment_pkey PRIMARY KEY (payment_id),
	CONSTRAINT payment_reservation_id_fkey FOREIGN KEY (reservation_id) REFERENCES public.reservation(reservation_id)
);

-- Table Triggers

create trigger set_timestamp before
update
    on
    public.payment for each row execute function trigger_set_timestamp();


-- public.madtrins_payment definition

-- Drop table

-- DROP TABLE public.madtrins_payment;

CREATE TABLE public.madtrins_payment (
	payment_id int4 NOT NULL,
	transaction_madtrins_id varchar(50) NOT NULL,
	payment_status varchar(50) NOT NULL,
	payment_type varchar(50) NOT NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	deleted_at timestamptz NULL,
	CONSTRAINT madtrins_payment_pkey PRIMARY KEY (payment_id),
	CONSTRAINT madtrins_payment_payment_id_fkey FOREIGN KEY (payment_id) REFERENCES public.payment(payment_id)
);