import {
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { PG_CONNECTION } from '../db/dbToken';
import { BookingDto } from '../dto/booking.dto';
import {
  BaggageStatus,
  BoardingStatus,
  PaymentMethod,
  PaymentStatus,
  ReservationStatus,
  SeatStatus,
} from '../enums/collection.enum';
import { Pool } from 'pg';
import { PaymentDto } from '../dto/payment.dto';

@Injectable()
export class BookingRepository {
  constructor(@Inject(PG_CONNECTION) private readonly pool: Pool) {}

  async bookSeat(bookingDto: BookingDto) {
    const client = await this.pool.connect();

    try {
      // begin transaction
      await client.query('BEGIN');

      // perform exclusive lock on the data row of a seat_id
      const lockSeatQuery = `SELECT seat_status FROM seat WHERE seat_id = $1 AND deleted_at is null FOR UPDATE;`;
      const lockSeatParam = [bookingDto.reservation.seat_id];
      const seatStatus = await client.query(lockSeatQuery, lockSeatParam);

      if (seatStatus?.rows[0]?.seat_status != 'IDLE') {
        throw new Error('can not booked non-idle seat');
      }
      // insert into table passenger
      const passengerInsertQuery = `INSERT INTO passenger(
        first_name,
        last_name,
        email,
        phone_number,
        nik,
        boarding_status)
        VALUES ($1, $2, $3, $4, $5, $6) RETURNING passenger_id;`;

      const passengerInsertParams = [
        bookingDto.passenger.first_name,
        bookingDto.passenger.last_name,
        bookingDto.passenger.email,
        bookingDto.passenger.phone_number,
        bookingDto.passenger.nik,
        BoardingStatus.PENDING,
      ];
      const insertPassenger = await client.query(
        passengerInsertQuery,
        passengerInsertParams,
      );
      const passengerId = insertPassenger.rows[0]['passenger_id'];

      // insert into table baggage
      const baggageInsertQuery = `INSERT INTO baggage (passenger_id, baggage_status) VALUES ($1, $2) RETURNING baggage_id;`;
      const baggageInsertParam = [passengerId, BaggageStatus.PENDING];
      const insertBaggage = await client.query(
        baggageInsertQuery,
        baggageInsertParam,
      );

      // insert into table reservation
      const reservationInsertQuery = `INSERT INTO reservation (
        flight_id,
        passenger_id,
        transaction_id,
        booking_code,
        reservation_status,
        seat_id,
        user_id
        ) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING reservation_id;`;
      const reservationInsertParams = [
        bookingDto.reservation.flight_id,
        passengerId,
        bookingDto.reservation.transaction_id,
        bookingDto.reservation.booking_code,
        ReservationStatus.BOOKED,
        bookingDto.reservation.seat_id,
        bookingDto.reservation.user_id,
      ];
      const reservationId = await client.query(
        reservationInsertQuery,
        reservationInsertParams,
      );

      // insert into table payment
      const paymentInsertQuery = `INSERT INTO payment (reservation_id) VALUES ($1) RETURNING payment_id;`;
      const paymentInsertParam = [reservationId.rows[0]['reservation_id']];
      const insertPayment = await client.query(
        paymentInsertQuery,
        paymentInsertParam,
      );

      // update value for corresponding seat_number to be 'booked'
      const seatUpdateQuery = `UPDATE seat SET seat_status = $1 WHERE seat_id = $2 RETURNING seat_id`;
      const seatUpdateParams = [
        SeatStatus.BOOKED,
        bookingDto.reservation.seat_id,
      ];
      const updateSeat = await client.query(seatUpdateQuery, seatUpdateParams);

      // commit transaction
      await client.query('COMMIT');

      return {
        passenger_id: insertPassenger.rows[0]['passenger_id'],
        baggage_id: insertBaggage.rows[0]['baggage_id'],
        reservation_id: reservationId.rows[0]['reservation_id'],
        payment_id: insertPayment.rows[0]['payment_id'],
        seat_id: updateSeat.rows[0]['seat_id'],
      };
    } catch (error) {
      await client.query('ROLLBACK');
      throw new InternalServerErrorException(error);
    } finally {
      client.release();
    }
  }

  async makePayment(paymentDto: PaymentDto) {
    const client = await this.pool.connect();

    try {
      // begin transaction
      await client.query('BEGIN');

      // search data associated with booking code
      // perform exclusive lock to the row in reservation table
      const reservationSearchQuery = `SELECT reservation_id, seat_id FROM reservation WHERE booking_code = $1 AND deleted_at is null FOR UPDATE;`;
      const reservationSearchParam = [paymentDto.booking_code];
      const data = await client.query(
        reservationSearchQuery,
        reservationSearchParam,
      );
      const reservationId = data.rows[0]['reservation_id'];
      const seatId = data.rows[0]['seat_id'];

      // lock row for seat
      // perform exclusive lock on the data row of a seat_id
      const lockSeatQuery = `SELECT seat_status, price FROM seat WHERE seat_id = $1 AND deleted_at is null FOR UPDATE;`;
      const lockSeatParam = [seatId];
      const seatInfo = await client.query(lockSeatQuery, lockSeatParam);

      if (seatInfo?.rows[0]?.seat_status != 'BOOKED') {
        throw new Error('unable processing payment of a non-booked seat');
      }

      // update seat_status of coresponding seat_id
      const updateSeatQuery = `UPDATE seat SET seat_status = $1 WHERE seat_id = $2`;
      const updateSeatParam = [SeatStatus.PAID, seatId];
      await client.query(updateSeatQuery, updateSeatParam);

      // update reservation_status on data row of reservation_id
      const updateReservationQuery = `UPDATE reservation SET reservation_status = $1 WHERE reservation_id = $2;`;
      const updateReservationParam = [ReservationStatus.PAID, reservationId];
      await client.query(updateReservationQuery, updateReservationParam);

      // perform exclusive lock on data row of payment_id
      const paymentLockQuery = `SELECT payment_id FROM payment WHERE reservation_id = $1 AND deleted_at is null FOR UPDATE;`;
      const paymentLockParam = [reservationId];
      const result = await client.query(paymentLockQuery, paymentLockParam);
      const paymentId = result.rows[0]['payment_id'];

      // update column reservation_code, payment_date and amount in payment table
      const paymentUpdateQuery = `
      UPDATE payment 
        SET 
          reservation_code = $1, 
          payment_method = $2,
          payment_date = $3,
          amount = $4
      WHERE payment_id = $5;`;
      const paymentUpdateParam = [
        paymentDto.reservation_code,
        PaymentMethod.MADTRINS,
        paymentDto.payment_date,
        seatInfo.rows[0].price,
        paymentId,
      ];
      await client.query(paymentUpdateQuery, paymentUpdateParam);

      // insert payment detail in table madtrins_payment
      const madtrinsInsertQuery = `INSERT INTO madtrins_payment (
          payment_id,
          transaction_madtrins_id,
          payment_status,
          payment_type
          ) VALUES ( $1, $2, $3, $4) 
        RETURNING 
          payment_id,
          transaction_madtrins_id,
          payment_status,
          payment_type ;`;
      const madtrinsInsertParams = [
        paymentId,
        paymentDto.transaction_madtrins_id,
        PaymentStatus.PAID,
        paymentDto.payment_type,
      ];
      const paymentResponse = await client.query(
        madtrinsInsertQuery,
        madtrinsInsertParams,
      );

      // commit transaction
      await client.query('COMMIT');

      return paymentResponse.rows;
    } catch (error) {
      await client.query('ROLLBACK');
      throw new InternalServerErrorException(error);
    } finally {
      client.release();
    }
  }

  async compensateBookingTransaction(bookingCode) {
    const client = await this.pool.connect();
    const dateNow = new Date();
    try {
      // begin transaction
      await client.query('BEGIN');

      // search data associated with booking code
      // perform exclusive lock to the row in reservation table
      const reservationSearchQuery = `SELECT reservation_id, passenger_id, seat_id, transaction_id FROM reservation WHERE booking_code = $1 AND deleted_at IS NULL FOR UPDATE;`;
      const reservationSearchParam = [bookingCode];
      const data = await client.query(
        reservationSearchQuery,
        reservationSearchParam,
      );
      const reservationId = data.rows[0]['reservation_id'];
      const passengerId = data.rows[0]['passenger_id'];
      const seatId = data.rows[0]['seat_id'];
      const reservationCode = data.rows[0]['transaction_id'];

      // lock row for seat
      // perform exclusive lock on the data row of a seat_id
      const lockSeatQuery = `SELECT seat_status, price FROM seat WHERE seat_id = $1 AND deleted_at is null FOR UPDATE;`;
      const lockSeatParam = [seatId];
      const seatInfo = await client.query(lockSeatQuery, lockSeatParam);

      if (seatInfo?.rows[0]?.seat_status != 'BOOKED') {
        throw new Error('unable processing payment of a non-booked seat');
      }

      // update seat_status of coresponding seat_id
      const updateSeatQuery = `UPDATE seat SET seat_status = $1 WHERE seat_id = $2`;
      const updateSeatParam = [SeatStatus.IDLE, seatId];
      await client.query(updateSeatQuery, updateSeatParam);

      // update reservation_status on data row of reservation_id
      const updateReservationQuery = `UPDATE reservation SET deleted_at = $1 WHERE reservation_id = $2;`;
      const updateReservationParam = [dateNow, reservationId];
      await client.query(updateReservationQuery, updateReservationParam);

      // perform exclusive lock on data row of payment_id
      const paymentLockQuery = `SELECT payment_id FROM payment WHERE reservation_id = $1 AND deleted_at is null FOR UPDATE;`;
      const paymentLockParam = [reservationId];
      const result = await client.query(paymentLockQuery, paymentLockParam);
      const paymentId = result.rows[0]['payment_id'];

      // update deleted_at in payment table
      const paymentUpdateQuery = `
      UPDATE payment 
        SET 
          deleted_at = $1 
      WHERE payment_id = $2;`;
      const paymentUpdateParam = [dateNow, paymentId];
      await client.query(paymentUpdateQuery, paymentUpdateParam);

      // update deleted_at table passenger
      const passengerUpdateQuery = `
      UPDATE passenger 
        SET 
          deleted_at = $1 
      WHERE passenger_id = $2;`;
      const passengerUpdateParam = [dateNow, passengerId];
      await client.query(passengerUpdateQuery, passengerUpdateParam);

      // update deleted_at table baggage
      const baggageUpdateQuery = `
      UPDATE baggage 
        SET 
          deleted_at = $1 
      WHERE passenger_id = $2;`;
      const baggageUpdateParam = [dateNow, passengerId];
      await client.query(baggageUpdateQuery, baggageUpdateParam);

      // commit transaction
      await client.query('COMMIT');

      return `SUCCESS COMPENSATE FOR BOOKING_CODE: ${bookingCode}`;
    } catch (error) {
      console.log([error], 'error compensate');
      await client.query('ROLLBACK');
      throw new InternalServerErrorException(error);
    } finally {
      client.release();
    }
  }

  async getReservationCode(bookingCode: string) {
    const resCodeQuery = `SELECT r.transaction_id FROM reservation r WHERE r.booking_code = $1 AND r.created_at IS NOT NULL;`;
    const resCodeParam = [bookingCode];
    const reservationCode = await this.pool.query(resCodeQuery, resCodeParam);
    return reservationCode?.rows[0]?.transaction_id;
  }

  async getPassengerEmail(bookingCode: string) {
    const passengerEmailQuery = `SELECT p.email, p.first_name, p.last_name FROM reservation r JOIN passenger p ON p.passenger_id = r.passenger_id WHERE r.booking_code = $1 AND r.created_at IS NOT NULL;`;
    const passengerEmailParam = [bookingCode];
    const passengerEmail = await this.pool.query(
      passengerEmailQuery,
      passengerEmailParam,
    );
    return passengerEmail?.rows[0];
  }

  async getTicketDetail(reservCode: string) {
    const ticketDetailQuery = `WITH airportflightgroup AS (
      SELECT
          f1.flight_id,
          f1.departure_time,
          a.airport_name AS departure_airport_name,
          a2.airport_name AS destination_airport_name
      FROM
          airports a
      JOIN
          flight f1 ON f1.departure_airport_id = a.airport_id
      JOIN
          flight f2 ON f2.destination_airport_id = a.airport_id
      JOIN
          airports a2 ON f2.destination_airport_id = a2.airport_id
      GROUP BY
          f1.flight_id, f1.departure_time, a.airport_name, a2.airport_name
  )
  
  SELECT
      p.email,
      p.first_name,
      p.last_name,
      afg.flight_id,
      afg.departure_airport_name,
      afg.destination_airport_name,
      s.seat_number
  FROM
      reservation r
  JOIN
      airportflightgroup afg ON r.flight_id = afg.flight_id
  JOIN
      seat s ON s.seat_id = r.seat_id
  JOIN
      passenger p ON r.passenger_id = p.passenger_id
  where r.transaction_id = $1 and r.deleted_at IS NULL ;`;

    const ticketDetailParam = [reservCode];
    const ticketDetail = await this.pool.query(
      ticketDetailQuery,
      ticketDetailParam,
    );
    return ticketDetail?.rows[0];
  }

  async checkBookingTimestamp(bookingCode: string) {
    const checkBookingQuery = `SELECT r.created_at FROM reservation r WHERE r.booking_code = $1 ;`;
    const checkBookingParam = [bookingCode];
    const bookingCreatedAt = await this.pool.query(
      checkBookingQuery,
      checkBookingParam,
    );
    return bookingCreatedAt?.rows[0]?.created_at;
  }

  async checkReservCodeTimestamp(reservCode: string) {
    const checkReservCodeQuery = `SELECT r.created_at, f.departure_time FROM reservation r JOIN flight f ON r.flight_id = f.flight_id WHERE r.transaction_id = $1 AND r.deleted_at IS NULL ;`;
    const checkReservCodeParam = [reservCode];
    const reservCodeCreatedAt = await this.pool.query(
      checkReservCodeQuery,
      checkReservCodeParam,
    );
    return reservCodeCreatedAt?.rows[0];
  }

  async checkUserTransactionTimestamp(
    userId: number,
    dateStart: string,
    dateEnd: string,
  ) {
    const queryCheckUserTransaction = `SELECT count(r.reservation_id) AS total FROM reservation r WHERE r.user_id = $1 AND r.created_at >= $2 and r.created_at < $3;`;
    const paramCheckUserTransaction = [userId, dateStart, dateEnd];
    const totalTransaction = await this.pool.query(
      queryCheckUserTransaction,
      paramCheckUserTransaction,
    );
    return totalTransaction?.rows[0]?.total;
  }

  async checkUserInAFlight(userId: number, flightId: number) {
    const queryUserInAFlight = `SELECT count(r.reservation_id) AS total FROM reservation r WHERE r.user_id = $1 AND r.flight_id = $2;`;
    const paramUserInAFlight = [userId, flightId];
    const totalTransaction = await this.pool.query(
      queryUserInAFlight,
      paramUserInAFlight,
    );
    return totalTransaction?.rows[0]?.total;
  }

  async checkBlacklistStatus(userId: number) {
    const queryBlacklistStatus = `SELECT u.blacklist_status AS user_blacklist_status FROM "user" u WHERE u.id = $1;`;
    const paramBlacklistStatus = [userId];
    const userStatus = await this.pool.query(
      queryBlacklistStatus,
      paramBlacklistStatus,
    );
    return userStatus?.rows[0]['user_blacklist_status'];
  }

  async checkSeatPrice(bookCode: string) {
    const querySeatPrice = `SELECT s.price AS seat_price FROM reservation r JOIN seat s ON r.seat_id = s.seat_id WHERE r.booking_code = $1;`;
    const paramSeatPrice = [bookCode];
    const seatPrice = await this.pool.query(querySeatPrice, paramSeatPrice);
    return seatPrice?.rows[0]['seat_price'];
  }
}
