import { Injectable } from '@nestjs/common';
import { BookingRepository } from './booking.repository';
import { BookingDto } from '../dto/booking.dto';
import { PaymentDto } from '../dto/payment.dto';
import { BookingTransactionDto } from '../dto/booking-transaction.dto';

@Injectable()
export class BookingService {
  constructor(private readonly bookingRepo: BookingRepository) {}

  async bookSeat(bookingDto: BookingDto) {
    return await this.bookingRepo.bookSeat(bookingDto);
  }

  async makePayment(paymentDto: PaymentDto) {
    return await this.bookingRepo.makePayment(paymentDto);
  }

  async compensateBookingTransaction(bookingCode: string) {
    return await this.bookingRepo.compensateBookingTransaction(bookingCode);
  }

  async getReservationCode(bookingCode: string) {
    return await this.bookingRepo.getReservationCode(bookingCode);
  }

  async getPassengerEmail(bookingCode: string) {
    return await this.bookingRepo.getPassengerEmail(bookingCode);
  }

  async getTicketDetail(reservCode) {
    return await this.bookingRepo.getTicketDetail(reservCode);
  }

  async checkBookingTimestamp(bookingCode: string) {
    return await this.bookingRepo.checkBookingTimestamp(bookingCode);
  }

  async checkReservCodeTimestamp(reservCode: string) {
    return await this.bookingRepo.checkReservCodeTimestamp(reservCode);
  }

  async checkUserTransactionTimestamp(
    userId: number,
    dateStart: string,
    dateEnd: string,
  ) {
    return await this.bookingRepo.checkUserTransactionTimestamp(
      userId,
      dateStart,
      dateEnd,
    );
  }

  async checkUserInAFlight(userId: number, flightId: number) {
    return await this.bookingRepo.checkUserInAFlight(userId, flightId);
  }

  async checkBlacklistStatus(data: BookingTransactionDto) {
    const result = await this.bookingRepo.checkBlacklistStatus(
      data.reservation.user_id,
    );
    data.reservation['user_blacklist_status'] = result;
    return data;
  }

  async checkSeatPrice(bookCode: string) {
    return await this.bookingRepo.checkSeatPrice(bookCode);
  }
}
