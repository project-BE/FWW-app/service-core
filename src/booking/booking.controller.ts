import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { BookingDto } from '../dto/booking.dto';
import { BookingService } from './booking.service';
import { PaymentDto } from '../dto/payment.dto';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { BookingTransactionDto } from '../dto/booking-transaction.dto';

@Controller('book')
export class BookingController {
  constructor(private readonly bookingService: BookingService) {}
  @Post('/seat')
  async bookSeat(@Body() bookingDto: BookingDto) {
    return this.bookingService.bookSeat(bookingDto);
  }

  @Post('/payment')
  async makePayment(@Body() paymentDto: PaymentDto) {
    return this.bookingService.makePayment(paymentDto);
  }

  @Get('/rollback')
  async compensateBookingTransaction(
    @Query('bookingCode') bookingCode: string,
  ) {
    return this.bookingService.compensateBookingTransaction(bookingCode);
  }

  @Get('/reservationcode')
  async getReservationCode(@Query('bookingCode') bookingCode: string) {
    return this.bookingService.getReservationCode(bookingCode);
  }

  @Get('/passengeremail')
  async getPassengerEmail(@Query('bookingCode') bookingCode: string) {
    return this.bookingService.getPassengerEmail(bookingCode);
  }

  @MessagePattern('get-ticket-detail')
  async getTicketDetail(@Payload() data) {
    return this.bookingService.getTicketDetail(data.reservation_code);
  }

  @MessagePattern('check-user-transaction')
  async checkUserTransactionTimestamp(@Payload() data: BookingTransactionDto) {
    const userId = data.reservation.user_id;
    const dateNow = new Date();
    const dateNowBeginning = new Date(
      `${dateNow.getFullYear()}-${dateNow.getMonth() + 1}-${dateNow.getDate()}`,
    );
    const dateStart = this.formatDateForPostgres(dateNowBeginning);
    const dateEnd = this.formatDateForPostgres(
      new Date(dateNowBeginning.setDate(dateNowBeginning.getDate() + 1)),
    );

    const totalUserTransaction =
      await this.bookingService.checkUserTransactionTimestamp(
        userId,
        dateStart,
        dateEnd,
      );

    const result = {
      ...data,
      ...(data.reservation['user_transaction_today'] = totalUserTransaction),
    };
    return result;
  }

  @MessagePattern('check-user-flight')
  async checkUserInAFlight(@Payload() data: BookingTransactionDto) {
    const totalUser = await this.bookingService.checkUserInAFlight(
      data.reservation.user_id,
      data.reservation.flight_id,
    );
    const result = {
      ...data,
      ...(data.reservation['user_in_flight'] = totalUser),
    };
    return result;
  }

  @MessagePattern('check-blacklist')
  async checkBlacklistStatus(@Payload() data: BookingTransactionDto) {
    return this.bookingService.checkBlacklistStatus(data);
  }

  @MessagePattern('check-booking-timestamp')
  async checkBookingTimestamp(@Payload() data) {
    return this.bookingService.checkBookingTimestamp(
      data['payment_message']['booking_code'],
    );
  }

  @MessagePattern('check-reservcode-timestamp')
  async checkReservCodeTimestamp(@Payload() data) {
    return this.bookingService.checkReservCodeTimestamp(
      data['reservation_code'],
    );
  }

  @MessagePattern('check-seat-price')
  async checkSeatPrice(@Payload() data) {
    return this.bookingService.checkSeatPrice(
      data['payment_message']['booking_code'],
    );
  }

  private formatDateForPostgres(date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
    const timezoneOffset = -date.getTimezoneOffset();
    const timezoneOffsetHours = Math.abs(Math.floor(timezoneOffset / 60))
      .toString()
      .padStart(2, '0');
    const timezoneOffsetMinutes = (Math.abs(timezoneOffset) % 60)
      .toString()
      .padStart(2, '0');
    const timezoneSign = timezoneOffset >= 0 ? '+' : '-';

    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}${timezoneSign}${timezoneOffsetHours}:${timezoneOffsetMinutes}`;
  }
}
