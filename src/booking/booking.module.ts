import { Module } from '@nestjs/common';
import { BookingController } from './booking.controller';
import { BookingService } from './booking.service';
import { DbModule } from '../db/db.module';
import { BookingRepository } from './booking.repository';

@Module({
  imports: [DbModule],
  controllers: [BookingController],
  providers: [BookingService, DbModule, BookingRepository],
})
export class BookingModule {}
