import { Test, TestingModule } from '@nestjs/testing';
import { FlightInquiryController } from './flight-inquiry.controller';

describe('FlightInquiryController', () => {
  let controller: FlightInquiryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FlightInquiryController],
    }).compile();

    controller = module.get<FlightInquiryController>(FlightInquiryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
