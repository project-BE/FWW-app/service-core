import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { FlightInquiryService } from './flight-inquiry.service';
import { FlightInquiryDto } from '../dto/flight-inquiry.dto';
import { MessagePattern, Payload } from '@nestjs/microservices';

@Controller('flight-inquiry')
export class FlightInquiryController {
  constructor(private readonly flightInquiryService: FlightInquiryService) {}

  @Get('/airports')
  async getAirportInfo() {
    return this.flightInquiryService.getAirportInfo();
  }

  @Post('/flights')
  async searchFlight(@Body() flightInquiryDto: FlightInquiryDto) {
    return this.flightInquiryService.searchFlight(flightInquiryDto);
  }

  @Get('/seats')
  async searchSeat(@Query('flightId') flightId) {
    return this.flightInquiryService.searchSeat(flightId);
  }

  @MessagePattern('search-airports')
  async _getAirportInfo() {
    return this.flightInquiryService.getAirportInfo();
  }

  @MessagePattern('search-flights')
  async _searchFlight(@Payload() flightInquiryDto: FlightInquiryDto) {
    return this.flightInquiryService.searchFlight(flightInquiryDto);
  }

  @MessagePattern('search-seats')
  async _searchSeat(@Payload() flightId) {
    return this.flightInquiryService.searchSeat(flightId);
  }
}
