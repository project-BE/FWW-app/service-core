import { Test, TestingModule } from '@nestjs/testing';
import { FlightInquiryService } from './flight-inquiry.service';

describe('FlightInquiryService', () => {
  let service: FlightInquiryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FlightInquiryService],
    }).compile();

    service = module.get<FlightInquiryService>(FlightInquiryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
