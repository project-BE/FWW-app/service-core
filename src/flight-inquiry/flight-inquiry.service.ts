import { Injectable } from '@nestjs/common';
import { FlightInquiryRepository } from './flight-inquiry.repository';
import { FlightInquiryDto } from '../dto/flight-inquiry.dto';

@Injectable()
export class FlightInquiryService {
  constructor(private readonly flightInquiryRepo: FlightInquiryRepository) {}

  async getAirportInfo() {
    return this.flightInquiryRepo.getAirportInfo();
  }

  async searchFlight(flightInquiryDto: FlightInquiryDto) {
    return this.flightInquiryRepo.searchFlight(flightInquiryDto);
  }

  async searchSeat(flightId: number) {
    return this.flightInquiryRepo.searchSeat(flightId);
  }
}
