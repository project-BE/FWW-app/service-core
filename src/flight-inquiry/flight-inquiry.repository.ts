import { Inject, Injectable } from '@nestjs/common';
import { PG_CONNECTION } from '../db/dbToken';
import { FlightInquiryDto } from '../dto/flight-inquiry.dto';

@Injectable()
export class FlightInquiryRepository {
  constructor(@Inject(PG_CONNECTION) private pool: any) {}

  async getAirportInfo() {
    const queryString = `select a.airport_id, a.airport_code, a.airport_name, a.cityname from airports a`;
    const result = await this.pool.query(queryString);
    return result.rows;
  }

  async searchFlight(flightInquiryDto: FlightInquiryDto) {
    const queryString = `SELECT
            f.flight_id,
            f.flight_code,
            f.departure_airport_id,
            dep.airport_name AS departure_airport_name,
            f.destination_airport_id,
            dest.airport_name AS destination_airport_name,
            f.departure_time,
            f.arrival_time,
            count (s.seat_status) as available_seats
        FROM
            public.flight f
        JOIN
            public.airports dep ON f.departure_airport_id = dep.airport_id
        JOIN
            public.airports dest ON f.destination_airport_id = dest.airport_id
        JOIN 
	        seat s on s.flight_id = f.flight_id 
        WHERE
            dep.cityname = $1 
            AND dest.cityname = $2
            AND f.departure_time >= $3
            AND s.seat_status = 'IDLE'
        group by f.flight_id, dep.airport_name, dest.airport_name
        ORDER BY f.departure_time ASC;`;

    const date = new Date(flightInquiryDto.departure_time);

    const convertedTime = `${date.getFullYear()}-${
      date.getMonth() + 1
    }-${date.getDate()}`;
    const params = [
      flightInquiryDto.departure_airport,
      flightInquiryDto.destination_airport,
      convertedTime,
    ];

    const result = await this.pool.query(queryString, params);
    return result.rows;
  }

  async searchSeat(flightId: number) {
    const queryString = `SELECT 
            f.flight_id,
            flight_code,
            s.seat_id,
            s.seat_number,
            s.price 
        from flight f
        join seat s on f.flight_id = s.flight_id 
        where s.flight_id = $1
        order by s.seat_number asc`;

    const result = await this.pool.query(queryString, [flightId]);
    return result.rows;
  }
}
