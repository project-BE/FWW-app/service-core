import { Module } from '@nestjs/common';
import { FlightInquiryController } from './flight-inquiry.controller';
import { FlightInquiryService } from './flight-inquiry.service';
import { DbModule } from '../db/db.module';
import { FlightInquiryRepository } from './flight-inquiry.repository';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from '../transport/kafka.config';

@Module({
  imports: [DbModule, ClientsModule.register(kafkaOptions)],
  controllers: [FlightInquiryController],
  providers: [FlightInquiryService, DbModule, FlightInquiryRepository],
})
export class FlightInquiryModule {}
