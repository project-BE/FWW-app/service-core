export enum ReservationStatus {
  BOOKED = 'BOOKED',
  PAID = 'PAID',
}

export enum SeatStatus {
  IDLE = 'IDLE',
  BOOKED = 'BOOKED',
  PAID = 'PAID',
}

export enum PaymentStatus {
  PENDING = 'PENDING',
  PAID = 'PAID',
}

export enum BoardingStatus {
  PENDING = 'PENDING',
  CHECKED_IN = 'CHECKED_IN',
  BOARDING = 'BOARDING',
}

export enum BaggageStatus {
  PENDING = 'PENDING',
  CHECKED_IN = 'CHECKED_IN',
  BOARDING = 'BOARDING',
  NO_BAGGAGE = 'NO_BAGGAGE',
}

export enum PaymentMethod {
  MADTRINS = 'MADTRINS',
}
