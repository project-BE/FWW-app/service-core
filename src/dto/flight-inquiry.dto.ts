export class FlightInquiryDto {
  departure_airport: string;
  destination_airport: string;
  departure_time: Date;
}
