export class PaymentDto {
  booking_code: string;
  reservation_code: string;
  transaction_madtrins_id: string;
  payment_type: string;
  payment_date: Date;
}
