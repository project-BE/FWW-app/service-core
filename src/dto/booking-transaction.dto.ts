export class BookingTransactionDto {
  transaction_id: string;

  reservation: Reservation;

  passenger: Passenger;
}

class Reservation {
  transaction_id: string;
  
  flight_id: number;

  departure_time: Date;

  seat_id: number;

  user_id: number;
}

class Passenger {
  first_name: string;

  last_name: string;

  email: string;

  phone_number: string;

  nik: string;
}
