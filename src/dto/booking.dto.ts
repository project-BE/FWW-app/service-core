export class BookingDto {
  reservation: Reservation;
  passenger: Passenger;
}

class Reservation {
  flight_id: string;
  transaction_id: string;
  booking_code: string;
  seat_id: number;
  user_id: number;
}

class Passenger {
  first_name: string;
  last_name: string;
  email: string;
  phone_number: string;
  nik: string;
}
