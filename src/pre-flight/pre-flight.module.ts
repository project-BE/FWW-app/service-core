import { Module } from '@nestjs/common';
import { PreFlightController } from './pre-flight.controller';
import { PreFlightService } from './pre-flight.service';
import { DbModule } from '../db/db.module';
import { PreFlightRepository } from './pre-flight.repository';

@Module({
  imports: [DbModule],
  controllers: [PreFlightController],
  providers: [PreFlightService, DbModule, PreFlightRepository],
})
export class PreFlightModule {}
