import { Inject, Injectable, InternalServerErrorException } from '@nestjs/common';
import { PG_CONNECTION } from '../db/dbToken';
import {
  BaggageStatus,
  BoardingStatus,
  PaymentStatus,
} from '../enums/collection.enum';

@Injectable()
export class PreFlightRepository {
  constructor(@Inject(PG_CONNECTION) private readonly pool: any) {}

  async redeemTicket(reservationCode: string) {
    const client = await this.pool.connect();

    try {
      await client.query('BEGIN');
      // validate payment_status (madtrins_payment)
      const searchPaymentQuery = `SELECT
            mp.payment_status, r.passenger_id
        FROM madtrins_payment mp 
        JOIN payment p ON p.payment_id = mp.payment_id
        JOIN reservation r ON r.reservation_id = p.reservation_id
        WHERE p.reservation_code  = $1;`;
      const searchPaymemtParam = [reservationCode];
      const data = await client.query(searchPaymentQuery, searchPaymemtParam);

      // validate payment status
      if (data?.rows[0]?.payment_status !== PaymentStatus.PAID) {
        throw new Error('unable processing redeem unpaid ticket ');
      }
      // perform exclusive lock on passenger table and baggage table
      const passengerLockQuery = `SELECT p.boarding_status FROM passenger p WHERE p.passenger_id = $1 FOR UPDATE;`;
      const baggageLockQuery = ` SELECT b.baggage_status FROM baggage b WHERE b.passenger_id = $1 FOR UPDATE;`;
      const passengerInfo = await client.query(passengerLockQuery, [
        data?.rows[0]?.passenger_id,
      ]);
      const baggageInfo = await client.query(baggageLockQuery, [
        data?.rows[0]?.passenger_id,
      ]);

      // check calidation for status boarding & baggage
      if (
        passengerInfo?.rows[0]?.boarding_status !== BoardingStatus.PENDING ||
        baggageInfo?.rows[0]?.baggage_status != BaggageStatus.PENDING
      ) {
        throw new Error('unable to process un-idle status baggage or boarding');
      }

      // update boarding_status & baggage_status
      const passengerUpdateQuery = `UPDATE passenger set boarding_status = $1 WHERE passenger_id = $2 RETURNING boarding_status;`;
      const passengerUpdateParams = [
        BoardingStatus.BOARDING,
        data?.rows[0]?.passenger_id,
      ];
      const updatePassenger = await client.query(
        passengerUpdateQuery,
        passengerUpdateParams,
      );

      const baggageUpdateQuery = `UPDATE baggage set baggage_status = $1 WHERE passenger_id = $2 RETURNING baggage_status;`;
      const baggageUpdateParams = [
        BaggageStatus.BOARDING,
        data?.rows[0]?.passenger_id,
      ];
      const updateBaggage = await client.query(
        baggageUpdateQuery,
        baggageUpdateParams,
      );

      await client.query('COMMIT');
      return {
        boarding_status: updatePassenger?.rows[0]['boarding_status'],
        baggage_status: updateBaggage?.rows[0]['baggage_status'],
      };
    } catch (error) {
      await client.query('ROLLBACK');
      throw new InternalServerErrorException(error);
    } finally {
      client.release();
    }
  }
}
