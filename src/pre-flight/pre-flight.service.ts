import { Injectable } from '@nestjs/common';
import { PreFlightRepository } from './pre-flight.repository';

@Injectable()
export class PreFlightService {
  constructor(private readonly preFlightRepo: PreFlightRepository) {}

  async redeemTicket(reservationCode: string) {
    return this.preFlightRepo.redeemTicket(reservationCode);
  }
}
