import { Test, TestingModule } from '@nestjs/testing';
import { PreFlightController } from './pre-flight.controller';

describe('PreFlightController', () => {
  let controller: PreFlightController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PreFlightController],
    }).compile();

    controller = module.get<PreFlightController>(PreFlightController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
