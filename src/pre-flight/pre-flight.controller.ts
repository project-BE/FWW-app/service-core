import { Controller, Get, Query } from '@nestjs/common';
import { PreFlightService } from './pre-flight.service';

@Controller('pre-flight')
export class PreFlightController {
  constructor(private readonly preFlightService: PreFlightService) {}

  @Get('/boarding')
  async redeemTicket(@Query('reservationCode') reservationCode: string) {
    return this.preFlightService.redeemTicket(reservationCode);
  }
}
