import { Controller, Get, Query } from '@nestjs/common';
import { UserTransactionService } from './user-transaction.service';

@Controller('user-transaction')
export class UserTransactionController {
  constructor(
    private readonly userTransactionService: UserTransactionService,
  ) {}

  @Get('/bytimestamp')
  async getUserTransactionTimestamp(
    @Query('userId') userId: number,
    @Query('startDate') startDate: string,
    @Query('endDate') endDate: string,
  ) {
    return this.userTransactionService.getUserTransactionTimestamp(
      userId,
      startDate,
      endDate,
    );
  }

  @Get('byflight')
  async getFlightUserTransaction(
    @Query('flightId') flightId: number,
    @Query('userId') userId: number,
  ) {
    return this.userTransactionService.getFlightUserTransaction(
      flightId,
      userId,
    );
  }
}
