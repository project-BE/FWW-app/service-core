import { Injectable } from '@nestjs/common';
import { UserTransactionRepository } from './user-transaction.repository';

@Injectable()
export class UserTransactionService {
  constructor(
    private readonly userTransactionRepo: UserTransactionRepository,
  ) {}

  async getUserTransactionTimestamp(
    userId: number,
    startDate: string,
    endDate: string,
  ) {
    return this.userTransactionRepo.getUserTransactionTimestamp(
      userId,
      startDate,
      endDate,
    );
  }

  async getFlightUserTransaction(flightId: number, userId: number) {
    return this.userTransactionRepo.getFlightUserTransaction(flightId, userId);
  }
}
