import { Inject, Injectable } from '@nestjs/common';
import { PG_CONNECTION } from '../db/dbToken';

@Injectable()
export class UserTransactionRepository {
  constructor(@Inject(PG_CONNECTION) private readonly pool: any) {}

  async getUserTransactionTimestamp(
    userId: number,
    startDate: string,
    endDate: string,
  ) {
    const timeStampQuery = `SELECT 
        COUNT(r.created_at) AS total 
    FROM reservation r 
    WHERE r.user_id = $1 AND r.created_at >= $2 AND r.created_at < $3;`;
    const timeStampParam = [userId, startDate, endDate];
    const data = await this.pool.query(timeStampQuery, timeStampParam);

    return {
      user_id: userId,
      time_boundary: [startDate, endDate],
      total_transaction: data?.rows[0]?.total,
    };
  }

  async getFlightUserTransaction(flightId: number, userId: number) {
    const flightTransQuery = `SELECT COUNT(reservation_id) AS total 
        FROM reservation r 
        WHERE r.flight_id = $1 AND user_id = $2;`;
    const flightTransParams = [flightId, userId];
    const dataTransaction = await this.pool.query(
      flightTransQuery,
      flightTransParams,
    );

    return dataTransaction?.rows[0]?.total;
  }
}
