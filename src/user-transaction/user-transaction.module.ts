import { Module } from '@nestjs/common';
import { UserTransactionController } from './user-transaction.controller';
import { UserTransactionService } from './user-transaction.service';
import { DbModule } from '../db/db.module';
import { UserTransactionRepository } from './user-transaction.repository';

@Module({
  imports: [DbModule],
  controllers: [UserTransactionController],
  providers: [UserTransactionService, UserTransactionRepository, DbModule]
})
export class UserTransactionModule {}
