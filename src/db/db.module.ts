import { Module } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { PG_CONNECTION } from './dbToken';
import { Pool } from 'pg';

dotenv.config();

const dbProvider = {
  provide: PG_CONNECTION,
  useValue: new Pool({
    user: process.env.POSTGRES_USERNAME,
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DATABASE,
    password: process.env.POSTGRES_PASSWORD,
    port: Number(process.env.POSTGRES_PORT),
  }),
};
@Module({
  providers: [dbProvider],
  exports: [dbProvider],
})
export class DbModule {}
