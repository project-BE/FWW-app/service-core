import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DbModule } from './db/db.module';
import { FlightInquiryModule } from './flight-inquiry/flight-inquiry.module';
import { BookingModule } from './booking/booking.module';
import { PreFlightModule } from './pre-flight/pre-flight.module';
import { UserTransactionModule } from './user-transaction/user-transaction.module';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from './transport/kafka.config';

@Module({
  imports: [
    DbModule,
    FlightInquiryModule,
    BookingModule,
    PreFlightModule,
    UserTransactionModule,
    ClientsModule.register(kafkaOptions),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
