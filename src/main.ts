import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ResponseInterceptor } from './response/response.interceptor';
import { ConfigService } from '@nestjs/config';
import { Transport } from '@nestjs/microservices';

const configService = new ConfigService();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const microservice = app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: configService.get('KAFKA_CLIENT_ID'),
        brokers: [configService.get('KAFKA_BROKERS')],
      },
      consumer: {
        groupId: configService.get('KAFKA_GROUPID'),
      },
    },
  });
  app.useGlobalInterceptors(new ResponseInterceptor());
  await app.startAllMicroservices();
  await app.listen(3003);
}
bootstrap();
